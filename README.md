#Projet Biocontainers + Bioconda
Pour chaque paquet Conda :<br>
une image Docker<br>
une image Singularity dérivée de l’image Docker<br>
Génération automatique, au moment de la publication du paquet<br>
Image ultra minimale = busybox + le contenu du paquet<br>
Pérenne (soutenu par bioconda/biocontainers/galaxy)<br>


#Téléchargeable en HTTP : https://depot.galaxyproject.org/singularity/<br><br>
12 To d’images prêtes à l’emploi<br>
CVMFS : CernVM-FS, système de fichier distribué, télécharge à la demande<br>
Répertoire /cvmfs/singularity.galaxyproject.org/<br>
Cluster IFB core<br>
GenOuest<br>
… (autres possibles)<br>
